-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2018 at 08:49 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `CSV_DB`
--

-- --------------------------------------------------------

--
-- Table structure for table `Materiales`
--

CREATE TABLE IF NOT EXISTS `Materiales` (
  `Clave` int(4) NOT NULL DEFAULT '0',
  `Descripcion` varchar(19) DEFAULT NULL,
  `Costo` int(4) DEFAULT NULL,
  PRIMARY KEY (`Clave`),
  UNIQUE KEY `COL 1` (`Clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Materiales`
--

INSERT INTO `Materiales` (`Clave`, `Descripcion`, `Costo`) VALUES
(1000, 'Varilla 3/16', 100),
(1010, 'Varilla 4/32', 115),
(1020, 'Varilla 3/17', 130),
(1030, 'Varilla 4/33', 145),
(1040, 'Varilla 3/18', 160),
(1050, 'Varilla 4/34', 175),
(1060, 'Varilla 3/19', 190),
(1070, 'Varilla 4/35', 205),
(1080, 'Ladrillos rojos', 50),
(1090, 'Ladrillos grises', 35),
(1100, 'Block', 30),
(1110, 'Megablock', 40),
(1120, 'Sillar rosa', 100),
(1130, 'Sillar gris', 110),
(1140, 'Cantera blanca', 200),
(1150, 'Cantera gris', 1210),
(1160, 'Cantera rosa', 1420),
(1170, 'Cantera amarilla', 230),
(1180, 'Recubrimiento P1001', 200),
(1190, 'Recubrimiento P1010', 220),
(1200, 'Recubrimiento P1019', 240),
(1210, 'Recubrimiento P1028', 250),
(1220, 'Recubrimiento P1037', 280),
(1230, 'Cemento ', 300),
(1240, 'Arena', 200),
(1250, 'Grava', 100),
(1260, 'Gravilla', 90),
(1270, 'Tezontle', 80),
(1280, 'Tepetate', 34),
(1290, 'Tuber', 200),
(1300, 'Tuber', 210),
(1310, 'Tuber', 220),
(1320, 'Tuber', 230),
(1330, 'Tuber', 240),
(1340, 'Tuber', 250),
(1350, 'Tuber', 260),
(1360, 'Pintura C1010', 125),
(1370, 'Pintura B1020', 125),
(1380, 'Pintura C1011', 725),
(1390, 'Pintura B1021', 125),
(1400, 'Pintura C1011', 125),
(1410, 'Pintura B1021', 125),
(1420, 'Pintura C1012', 125),
(1430, 'Pintura B1022', 125);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
