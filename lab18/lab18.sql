

/*
La suma de las cantidades e importe total 
de todas las entregas realizadas durante el 97.


select sum(cantidad) as 'Total de materiales', sum(cantidad*costo*(1+PorcentajeImpuesto/100)) as 'Total de importe'
from materiales m, entregan e
where e.Clave = m.Clave
*/

/*
Para cada proveedor, obtener la raz�n social 
del proveedor, n�mero de entregas e importe 
total de las entregas realizadas.


select RazonSocial, count(cantidad) as 'N�m de entregas', sum(cantidad*costo) as 'Total de Importe'
from proveedores p, Entregan e, materiales m
where p.RFC=e.RFC
and m.Clave=e.Clave
group by RazonSocial

select * 
from entregan
*/

/*
Por cada material obtener la clave y descripci�n del material, 
la cantidad total entregada, la m�nima cantidad entregada, 
la m�xima cantidad entregada, el importe total 
de las entregas de aquellos materiales en los 
que la cantidad promedio entregada sea mayor a 400.

select e.clave, descripcion, sum(cantidad) as 'Total_entregados', min(cantidad) as 'M�nimo', MAX(cantidad) as 'M�ximo', sum(Cantidad*costo) as 'Total importe'
from materiales m, entregan e
where e.Clave=m.Clave
group by e.clave, descripcion
HAVING sum(cantidad)/count(cantidad)>400
*/


/*
Para cada proveedor, indicar su raz�n social 
y mostrar la cantidad promedio de cada 
material entregado, detallando la clave y 
descripci�n del material, excluyendo aquellos 
proveedores para los que la cantidad promedio sea menor a 500.

select razonSocial, e.Clave, Descripcion, sum(cantidad)/count(cantidad) as 'Cantidad promedio'
from Proveedores p, entregan e, Materiales m
where p.RFC=e.RFC
and m.Clave=e.Clave
group by RazonSocial, Descripcion, e.Clave
HAVING sum(cantidad)/count(cantidad)<370
or sum(cantidad)/count(cantidad)>450
*/

/*
Mostrar en una solo consulta los mismos datos 
que en la consulta anterior pero para dos grupos 
de proveedores: aquellos para los que la cantidad promedio 
entregada es menor a 370 y aquellos para los que 
la cantidad promedio entregada sea mayor a 450. 


select razonSocial, e.Clave, Descripcion, sum(cantidad)/count(cantidad) as 'Cantidad promedio'
from Proveedores p, entregan e, Materiales m
where p.RFC=e.RFC
and m.Clave=e.Clave
group by RazonSocial, Descripcion, e.Clave
HAVING sum(cantidad)/count(cantidad)<370
or sum(cantidad)/count(cantidad)>450
*/

/*Considerando que los valores de tipos CHAR y VARCHAR 
deben ir encerrados entre ap�strofes, los valores num�ricos 
se escriben directamente y los de fecha, como '1-JAN-00' 
para 1o. de enero del 2000, inserta cinco nuevos materiales. */

/*INSERT INTO Materiales VALUES
(1001, 'Varilla 3/10', 90, 1.01),
(1002, 'Varilla 3/11', 91, 1.02),
(1003, 'Varilla 3/12', 92, 1.03),
(1004, 'Varilla 3/13', 93, 1.04),
(1005, 'Varilla 3/14', 94, 1.05);

select * from Materiales
*/

/*Clave y descripci�n de los materiales 
que nunca han sido entregados.
*/
/*
select clave, descripcion
from Materiales m
where clave not in (select clave from entregan)
*/

/*Raz�n social de los proveedores que han 
realizado entregas tanto al proyecto 'Vamos M�xico' 
como al proyecto 'Quer�taro Limpio'.
*/
/*
select RazonSocial
from proveedores pr, Entregan e, Proyectos p
where pr.RFC=e.RFC and p.Numero=e.Numero
and p.Denominacion='Vamos Mexico'
and RazonSocial in ( select RazonSocial
						from proveedores pr, Entregan e, Proyectos p
						where pr.RFC=e.RFC and p.Numero=e.Numero
						and p.Denominacion='Queretaro Limpio')
*/

/*Descripci�n de los materiales que nunca han 
sido entregados al proyecto 'CIT Yucat�n'.
*/

/*Select Descripcion, Denominacion
from Materiales m, Entregan e, Proyectos pr
where m.Clave = e.Clave
and pr.Numero = e.Numero
and not Denominacion='CIT Yucatan'
*/




/*Raz�n social y promedio de cantidad entregada 
de los proveedores cuyo promedio de cantidad 
entregada es mayor al promedio de la cantidad entregada 
por el proveedor con el RFC 'VAGO780901'.
*/
/*select RazonSocial, sum(cantidad)/count(cantidad) as 'promedio'
from Entregan e, Proveedores p
where e.RFC=p.RFC
group by RazonSocial
having sum(cantidad)/count(cantidad)>(select sum(cantidad)/count(cantidad)
										from Entregan e, Proveedores p
										where e.RFC=p.RFC
										and e.RFC='VAGO780901')
										*/


/*RFC, raz�n social de los proveedores que participaron 
en el proyecto 'Infonavit Durango' y cuyas 
cantidades totales entregadas en el 2000 fueron 
mayores a las cantidades totales entregadas en el 2001.
*/
select e.RFC, RazonSocial, sum(cantidad)
from Proveedores prov, Proyectos proy, Entregan e
where prov.RFC=e.RFC
and proy.Numero=e.Numero
and Denominacion = 'Infonavit Durango'
and Fecha between '01-JAN-00' and '31-DEC-00'
group by e.RFC, RazonSocial
HAVING sum(cantidad)> (select sum(cantidad)
					from Proveedores prov, Proyectos proy, Entregan e
					where prov.RFC=e.RFC
					and proy.Numero=e.Numero
					and Denominacion = 'Infonavit Durango'
					and Fecha between '01-JAN-01' and '31-DEC-01')

