/* Instr 1
select * from materiales 
*/

--set dateformat dmy
/* Instr2
select * from materiales
where clave=1000
*/

/* Instr3
select clave,rfc,fecha from entregan 
*/

/* Instr 4
select * from materiales,entregan
where materiales.clave = entregan.clave
*/


/* Instr 5 
select * from entregan,proyectos
where entregan.numero < = proyectos.numero
*/

/* Instr 6
(select * from entregan where clave=1450)
union
(select * from entregan where clave=1300) 
*/
--select * from entregan where clave=1450 or clave=1300

/* Instr 7
(select clave from entregan where numero=5001)
intersect
(select clave from entregan where numero=5018)
*/

/* Instr 8
select * from entregan where not clave=1000
*/

/* Instr 9
select * from entregan,materiales
*/

/* Instr 10
set dateformat dmy
select Descripcion
from materiales m, entregan e 
where e.Clave=m.Clave
and Fecha between '01/01/00' and '31/12/00'
*/

/* Instr 11
set dateformat dmy
select distinct Descripcion
from materiales m, entregan e 
where e.Clave=m.Clave
and Fecha between '01/01/00' and '31/12/00'
*/

/* Instr 12
select P.numero, denominacion, Fecha, Cantidad
from Proyectos P, Entregan e
where P.Numero=e.Numero
order by P.numero, Fecha desc
*/

--SELECT * FROM Materiales where Descripcion LIKE 'Si%'

--SELECT * FROM Materiales where Descripcion LIKE 'Si'

/* Instr 13
DECLARE @foo varchar(40);
DECLARE @bar varchar(40);
SET @foo = '�Que resultado';
SET @bar = ' ���??? '
SET @foo += ' obtienes?';
PRINT @foo + @bar; 
*/

--SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%';

--SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%';

--SELECT Numero FROM Entregan WHERE Numero LIKE '___6';

/* Instr 14
set dateformat dmy
SELECT Clave,RFC,Numero,Fecha,Cantidad
FROM Entregan
WHERE Numero Between 5000 and 5010
and Fecha between '01/01/00' and '31/12/00';
*/

/* Instr 15
SELECT RFC,Cantidad, Fecha,Numero
FROM [Entregan]
WHERE [Numero] Between 5000 and 5010
And RFC not In ( SELECT [RFC]
FROM [Proveedores]
WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] )
*/


--Instr16
--SELECT TOP 2 * FROM Proyectos
--SELECT TOP Numero FROM Proyectos
--ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2);
--UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000;

--select * from materiales


/*

�Qu� consulta usar�as para obtener el importe 
de las entregas es decir, el total en dinero de lo entregado, 
basado en la cantidad de la entrega y el precio del material 
y el impuesto asignado? 

*/

/*
select sum((Cantidad*Costo)*(1+PorcentajeImpuesto))
from Entregan, Materiales
where entregan.Clave=Materiales.Clave
*/
/*
CREATE VIEW mats AS
select m.Clave, Descripcion
from Proyectos p, entregan e, materiales m
where p.Numero=e.Numero
and Denominacion='Mexico sin ti no estamos completos'
and m.Clave=e.Clave;


SELECT * FROM mats;  
GO  
*/


/*
CREATE VIEW acmeTools AS
select m.Clave, Descripcion
from Proveedores p, entregan e, materiales m
where p.RFC=e.RFC
and RazonSocial='Acme tools'
and m.Clave=e.Clave;

SELECT * FROM acmeTools;  
GO  
*/

/*
CREATE VIEW rfc2000 as
select e.rfc, sum(cantidad) as 'cantidad_total'
from Proveedores p, entregan e
where Fecha between '01/01/00' and '31/12/00'
and p.RFC=e.RFC
group by e.rfc
having sum(cantidad) > 300


select * from rfc2000
*/
/*
CREATE VIEW ubpatron as
select * 
from materiales
where Descripcion like '%ub%'

select * from ubpatron
*/

/*
CREATE VIEW ultimacons as
select m.descripcion, sum(cantidad) as 'total_cant', sum(cantidad*costo) as 'costo'
from Materiales m, entregan e
where m.Clave=e.Clave
group by descripcion


select * from ultimacons

*/

/*CREATE VIEW penultimaKonz as
select costo, descripcion, Denominacion
from Materiales m, entregan e, proyectos p, Proveedores pr
where p.Numero=e.Numero
and m.Clave=e.Clave
and p.Denominacion='Televisa en acci�n'
and pr.RFC=e.RFC
and pr.RazonSocial in(select RazonSocial
						from entregan e, proyectos p, Proveedores pr
						where p.Numero=e.Numero
						and pr.RFC=e.RFC
						and p.Denominacion='Educando en Coahuila')


select * from penultimaKonz
*/

/*
create view konzultaM as
select Denominacion, e.RFC, RazonSocial
from Materiales m, entregan e, proyectos p, Proveedores pr
where p.Numero=e.Numero
and m.Clave=e.Clave
and p.Denominacion='Televisa en acci�n'
and pr.RFC=e.RFC
and pr.RazonSocial not in(select RazonSocial
						from entregan e, proyectos p, Proveedores pr
						where p.Numero=e.Numero
						and pr.RFC=e.RFC
						and p.Denominacion='Educando en Coahuila')




select * from konzultaM
*/

/*
select denominacion, sum(cantidad*costo)
from entregan e, proveedores pro, proyectos pre, Materiales m
where e.Clave=m.Clave
and pro.RazonSocial=e.RFC
and pre.Numero=e.Numero
group by Denominacion



select Descripcion, sum(cantidad)
from materiales m, Entregan e
where m.Clave=e.Clave
and Fecha between '01/01/00' and '31/12/00'
group by Descripcion

*/
set dateformat dmy
select top 1 descripcion, cantidad
from Materiales m, Entregan e
where m.Clave=e.Clave
and fecha between '01/01/01' and '31/12/01'
order by cantidad desc








