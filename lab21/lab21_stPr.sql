/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'creaMaterial' AND type = 'P')
    DROP PROCEDURE creaMaterial
GO
            
CREATE PROCEDURE creaMaterial
    @uclave NUMERIC(5,0),
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2),
    @uimpuesto NUMERIC(6,2)
AS
    INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
GO
*/

/*
EXECUTE creaMaterial 5000,'Martillos Acme',250,15
Select * from Materiales
*/


--Por analog�a crea procedimientos almacenados con los siguientes objetivos:
/*
modificaMaterial que permite modificar un material que reciba como par�metros las columnas 
de la tabla materiales y actualice las columnas correspondientes con los valores recibidos, 
para el registro cuya llave sea la clave que se recibe como par�metro.
*/

/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'modificaMaterial' AND type = 'P')
    DROP PROCEDURE modificaMaterial
GO
            
CREATE PROCEDURE modificaMaterial
    @uclave NUMERIC(5,0),
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2),
    @uimpuesto NUMERIC(6,2)
AS
    UPDATE Materiales 
	SET clave = @uclave, descripcion = @udescripcion, costo = @ucosto, PorcentajeImpuesto = @uimpuesto
	WHERE clave = @uclave
GO

EXECUTE modificaMaterial 5000,'Martillos Acme',250,10
select * from materiales
*/


/*
eliminaMaterial que elimina el registro de la tabla materiales cuya llave 
sea la clave que se recibe como par�metro.
*/

/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'eliminaMaterial' AND type = 'P')
    DROP PROCEDURE eliminaMaterial
GO
            
CREATE PROCEDURE eliminaMaterial
    @uclave NUMERIC(5,0)
AS
    DELETE FROM Materiales 
	WHERE clave = @uclave
GO

EXECUTE eliminaMaterial 5000
select * from materiales
*/

/*
Desarrollar los procedimientos (almacenados) creaProyecto , modificaproyecto y eliminaproyecto, 
hacer lo mismo para las tablas proveedores y entregan.
*/

--PROYECTO

/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'creaProyecto' AND type = 'P')
    DROP PROCEDURE creaProyecto
GO
            
CREATE PROCEDURE creaProyecto
    @unumero NUMERIC(5,0),
    @udenominacion VARCHAR(50)
AS
    INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
GO



IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'modificaProyecto' AND type = 'P')
    DROP PROCEDURE modificaProyecto
GO
            
CREATE PROCEDURE modificaProyecto
	@unumero NUMERIC(5,0),
    @udenominacion VARCHAR(50)
AS
    UPDATE Proyectos
	SET Numero = @unumero, Denominacion = @udenominacion
	WHERE Numero = @unumero
GO




IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'eliminaProyecto' AND type = 'P')
    DROP PROCEDURE eliminaProyecto
GO
            
CREATE PROCEDURE eliminaProyecto
    @unumero NUMERIC(5,0)
AS
    DELETE FROM Proyectos 
	WHERE Numero = @unumero
GO
*/


--proveedores

/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'creaProveedor' AND type = 'P')
    DROP PROCEDURE creaProveedor
GO
            
CREATE PROCEDURE creaProveedor
    @urfc VARCHAR(20),
	@urazonsocial VARCHAR(50)
AS
    INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
GO





IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'modificaProveedor' AND type = 'P')
    DROP PROCEDURE modificaProveedor
GO
            
CREATE PROCEDURE modificaProveedor
    @urfc VARCHAR(20),
	@urazonsocial VARCHAR(50)
AS
    UPDATE Proveedores 
	SET RazonSocial = @urazonsocial
	WHERE rfc= @urfc
GO




IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'eliminaProveedor' AND type = 'P')
    DROP PROCEDURE eliminaProveedor
GO
            
CREATE PROCEDURE eliminaProveedor
    @urfc VARCHAR(20)
AS
    DELETE FROM Proveedores 
	WHERE rfc = @urfc
GO
*/








---entregan


/*
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'creaEntrega' AND type = 'P')
    DROP PROCEDURE creaEntrega
GO
            
CREATE PROCEDURE creaEntrega
    @uclave NUMERIC(5,0),
    @urfc VARCHAR(20),
	@unumero NUMERIC(5,0),
	@ufecha DATETIME,
	@ucantidad NUMERIC(5,0)
	
AS
    INSERT INTO Entregan VALUES(@uclave, @urfc, @unumero, @ufecha,@ucantidad)
GO


IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'modificaEntrega' AND type = 'P')
    DROP PROCEDURE modificaEntrega
GO
            
CREATE PROCEDURE modificaEntrega
    @uclave NUMERIC(5,0),
    @urfc VARCHAR(20),
	@unumero NUMERIC(5,0),
	@ufecha DATETIME,
	@ucantidad NUMERIC(5,0)
AS
    UPDATE Entregan 
	SET cantidad = @ucantidad
	WHERE clave = @uclave
	AND RFC = @urfc 
	AND numero = @unumero
	AND fecha = @ufecha
GO

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'eliminaEntrega' AND type = 'P')
    DROP PROCEDURE eliminaEntrega
GO
            
CREATE PROCEDURE eliminaEntrega
    @uclave NUMERIC(5,0),
    @urfc VARCHAR(20),
	@unumero NUMERIC(5,0),
	@ufecha DATETIME
AS
    DELETE FROM Entregan 
	WHERE clave = @uclave
	AND RFC = @urfc 
	AND numero = @unumero
	AND fecha = @ufecha
GO
*/


IF EXISTS (SELECT name FROM sysobjects 
    WHERE name = 'queryMaterial' AND type = 'P')
    DROP PROCEDURE queryMaterial
GO
                            
CREATE PROCEDURE queryMaterial
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2)
                            
AS
    SELECT * FROM Materiales WHERE descripcion 
    LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
GO














