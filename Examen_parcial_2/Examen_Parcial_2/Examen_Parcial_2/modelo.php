<?php

$tipos[]='Tipos';


    function connect() {
        $ENV = "prod";
        if ($ENV == "dev") {
            $mysql = mysqli_connect("localhost","pedro","perez","RaptorSystem");
                                            //root si estan en windows
        } else  if ($ENV == "prod"){
            $mysql = mysqli_connect("localhost","ablemus494","","RaptorSystem");
        }
        $mysql->set_charset("utf8");
        return $mysql;
    }
    



    function disconnect($mysql) {
        mysqli_close($mysql);
    }
    

    //registra un incidene
    function registraIncidente(){
        
        $lugar=$_GET["lugar"];
        $tipo=$_GET["tipo"];
        $db = connect();
        if ($db != NULL) {
            // insert command specification 
            $query='INSERT INTO incidentes (lugar, tipo) VALUES (?,?) ';
            // Preparing the statement 
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params 
            if (!$statement->bind_param("ss", $lugar, $tipo)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
             // Executing the statement
             if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
              }
            
            mysqli_free_result($results);
            disconnect($db);
            return true;
        } 
        return false;
    }


    //busca incidente por tipo
    function buscaTipo(){
        
        $tipounico=$_GET["tipounico"];
        $q =$_GET['q'];
        $db = connect();
        if ($db != NULL) {
            //Specification of the SQL query
            $query='SELECT * FROM incidentes WHERE tipo="'.$tipounico. '" ORDER BY fecha DESC';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '<div class="container">
                        <h2>Últimos Incidentes de tipo '.$q.'</h2>
                        <p>Listado de incidentes en Raptor Park</p>            
                        <table class="table table-striped">
                            <thead>
                                <tr>';
           $columnas = $results->fetch_fields();
           $html .= '<th>ID</th>';
           $html .= '<th>Lugar</th>';
           $html .= '<th>Tipo</th>';
           $html .= '<th>Fecha</th>';
           $html .= '</tr>';
           $html .= '</thead>';
           $html .= '<tbody>';
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {

                    $html .= '<tr>';
                    $html .= '<td>'.$fila["id"].'</td>'; 
                    $html .= '<td>'.$fila["lugar"].'</td>'; 
                    $html .= '<td>'.$fila["tipo"].'</td>';
                    $html .= '<td>'.$fila["fecha"].'</td>';
                    $html .= '</tr>';
            }
            $html .= '</tbody></table></div>';   
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }





    //cuenta el total de incidentes
    function cuentaIncidentes(){
        
        $db = connect();
        if ($db != NULL) {
        
            $query='SELECT COUNT(tipo) as "Total" FROM incidentes';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '';
           
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                
                        $html .= '<h2>Total de incidentes: '.$fila["Total"].'</h2><br><br>';
            }
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }





    function cuentaIncidentesTipo(){
        
        $db = connect();
        if ($db != NULL) {
            
            $tipounico=$_GET["tipounico"];
        
            $query='SELECT COUNT(tipo) as "Total" FROM incidentes WHERE tipo="'.$tipounico. '"';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '';
           
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                
                        $html .= '<h2>Total de incidentes: '.$fila["Total"].'</h2><br>';
            }
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }



    //obtiene todos los incidentes
    function getIncidentes(){
        $db = connect();
        if ($db != NULL) {
            
            //Specification of the SQL query
            $query='SELECT * FROM incidentes ORDER BY fecha DESC';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '<div class="container">
                        <h2>Últimos Incidentes</h2>
                        <p>Listado de incidentes en Raptor Park</p>            
                        <table class="table table-striped">
                            <thead>
                                <tr>';
           $columnas = $results->fetch_fields();
           $html .= '<th>ID</th>';
           $html .= '<th>Lugar</th>';
           $html .= '<th>Tipo</th>';
           $html .= '<th>Fecha</th>';
           $html .= '</tr>';
           $html .= '</thead>';
           $html .= '<tbody>';
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {

                    $html .= '<tr>';
                    $html .= '<td>'.$fila["id"].'</td>'; 
                    $html .= '<td>'.$fila["lugar"].'</td>'; 
                    $html .= '<td>'.$fila["tipo"].'</td>';
                    $html .= '<td>'.$fila["fecha"].'</td>';
                    $html .= '</tr>';
            }
            $html .= '</tbody></table></div>';   
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }



    function getTipos(){
        
        $db = connect();
        if ($db != NULL) {
        
            $query='SELECT * FROM tipos ORDER BY nombre ASC';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '';
           
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                
                        $html .= '<option value="'.$fila["nombre"].'">'.$fila["nombre"].'</option>';
                        
            }
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }



    function getTiposRAW(){
        
        $db = connect();
        if ($db != NULL) {
            $query='SELECT * FROM tipos ORDER BY nombre ASC';
            $query;
            $results = $db->query($query);

           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                
                        //$html .= '<option value="'.$fila["nombre"].'">'.$fila["nombre"].'</option>';
                        $tipos[] = '<option value="'.$fila["nombre"].'">'.$fila["nombre"].'</option>';
            }
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }


    function getLugares(){
        
        $db = connect();
        if ($db != NULL) {
        
            $query='SELECT * FROM lugares ORDER BY nombre ASC';
            $query;
             // Query execution; returns identifier of the result group
            $results = $db->query($query);
             // cycle to explode every line of the results
           $html =  '';
           
           while ($fila = mysqli_fetch_array($results, MYSQLI_BOTH)) {
                
                        $html .= '<option value="'.$fila["nombre"].'">'.$fila["nombre"].'</option>';
            }
            echo $html;
            // it releases the associated results
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }


    function eliminarIncidente(){
        $db = connect();
        if ($db != NULL) {
            
            $id=$_GET["id"];
            
            // Deletion query construction
            $query = 'DELETE FROM incidentes WHERE id=?';
            if (!($statement = $db->prepare($query))) {
                die("The preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params
            if (!$statement->bind_param("s", $id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            } 
            // delete execution
            if ($statement->execute()) {
                echo 'There were ' . mysqli_affected_rows($db) . ' affected rows';
            } else {
                die("Update failed: (" . $statement->errno . ") " . $statement->error);
            }
            mysqli_free_result($results);
            disconnect($db);
            return true;
        }
        return false;
    }





?>