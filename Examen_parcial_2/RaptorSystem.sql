-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-04-2018 a las 16:37:15
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `RaptorSystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes`
--

CREATE TABLE IF NOT EXISTS `incidentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lugar` varchar(100) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `incidentes`
--

INSERT INTO `incidentes` (`id`, `fecha`, `lugar`, `tipo`) VALUES
(2, '2018-04-03 14:45:00', 'Centro Operativo', 'Auto descompuesto'),
(3, '2018-04-03 15:11:47', 'Centro turístico', 'Falla eléctrica'),
(4, '2018-04-03 15:11:51', 'Centro turístico', 'Fuga de hervíboro'),
(5, '2018-04-03 15:11:55', 'Centro turístico', 'Fuga de Velociraptors'),
(6, '2018-04-03 15:11:57', 'Centro turístico', 'Fuga de T-Rex'),
(7, '2018-04-03 15:12:00', 'Centro turístico', 'Robo de DNA'),
(8, '2018-04-03 15:12:03', 'Centro turístico', 'Visitante en zona no autorizada'),
(9, '2018-04-03 15:47:27', 'Centro Operativo', 'Fuga de hervíboro'),
(10, '2018-04-03 16:01:29', 'Centro Operativo', 'Auto descompuesto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE IF NOT EXISTS `lugares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `lugares`
--

INSERT INTO `lugares` (`id`, `nombre`) VALUES
(1, 'Centro turístico'),
(2, 'Laboratorios'),
(4, 'Restaurante'),
(5, 'Centro Operativo'),
(6, 'Triceratops'),
(7, 'Dilofosaurios'),
(8, 'Velociraptors'),
(9, 'T-Rex'),
(10, 'Planicie de Hervíboros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE IF NOT EXISTS `tipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id`, `nombre`) VALUES
(1, 'Fuga de hervíboro'),
(2, 'Fuga de Velociraptors'),
(3, 'Falla eléctrica'),
(5, 'Fuga de T-Rex'),
(6, 'Robo de DNA'),
(7, 'Auto descompuesto'),
(8, 'Visitante en zona no autorizada');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
