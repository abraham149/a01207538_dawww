

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Clientes_Banca')
	DROP TABLE Clientes_Banca

CREATE TABLE Clientes_Banca
(
  NoCuenta varchar(5) NOT NULL PRIMARY KEY,
  Nombre varchar(30),
  Saldo numeric(10,2)
)

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Tipos_movimiento')
	DROP TABLE Tipos_movimiento

CREATE TABLE Tipos_movimiento
(
  ClaveM varchar(2) NOT NULL PRIMARY KEY,
  Descripcion varchar(30)
)



IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Movimientos')
	DROP TABLE Movimientos

CREATE TABLE Movimientos
( 
  NoCuenta varchar(5) NOT NULL,
  ClaveM varchar(2) NOT NULL,
  Fecha datetime,
  Monto numeric(10,2),
  CONSTRAINT FK_NoCuenta FOREIGN KEY (NoCuenta) REFERENCES Clientes_Banca(NoCuenta),
  CONSTRAINT FK_ClaveM FOREIGN KEY (ClaveM) REFERENCES Tipos_movimiento(ClaveM)
)



BEGIN TRANSACTION PRUEBA1
INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1


 SELECT * FROM Clientes_Banca


BEGIN TRANSACTION PRUEBA2
INSERT INTO CLIENTES_BANCA VALUES('004','Ricardo Rios Maldonado',19000);
INSERT INTO CLIENTES_BANCA VALUES('005','Pablo Ortiz Arana',15000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Manuel Alvarado',18000); 


ROLLBACK TRANSACTION PRUEBA2 



BEGIN TRANSACTION PRUEBA3
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico');
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3


BEGIN TRANSACTION PRUEBA4
INSERT INTO MOVIMIENTOS VALUES('001','A',GETDATE(),500);
UPDATE CLIENTES_BANCA SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4


SELECT * FROM Movimientos







BEGIN TRANSACTION PRUEBA5
INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000);


IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END





SELECT * FROM Clientes_Banca









IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'REGISTRAR_RETIRO_CAJERO ' AND type = 'P')
    DROP PROCEDURE REGISTRAR_RETIRO_CAJERO 
GO
                            
CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO 
    @NoCuenta VARCHAR(5),
    @Monto NUMERIC(10,2)
                            
AS
	BEGIN TRANSACTION Retiro
    INSERT INTO MOVIMIENTOS VALUES(@NoCuenta,'A',GETDATE(),@Monto); 
	UPDATE CLIENTES_BANCA SET SALDO = SALDO - @Monto  WHERE NoCuenta=@NoCuenta
	
	IF @@ERROR = 0
	COMMIT TRANSACTION Retiro
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION Retiro
	END
GO



IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'REGISTRAR_DEPOSITO_VENTANILLA  ' AND type = 'P')
    DROP PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA  
GO
                            
CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA  
    @NoCuenta VARCHAR(5),
    @Monto NUMERIC(10,2)
                            
AS	
	BEGIN TRANSACTION Deposito
     INSERT INTO MOVIMIENTOS VALUES(@NoCuenta,'B',GETDATE(),@Monto); 
	 UPDATE CLIENTES_BANCA SET SALDO = SALDO + @Monto  WHERE NoCuenta=@NoCuenta
 	
	IF @@ERROR = 0
	COMMIT TRANSACTION Deposito
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION Deposito
	END
GO

















